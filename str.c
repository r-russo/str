#include "str.h"

#include <memory.h>
#include <stdio.h>
#include <stdlib.h>

Str str_create_empty() {
  return (Str){.chars = malloc(STR_DEFAULT_CAPACITY * sizeof(char)),
               .length = 0,
               .capacity = STR_DEFAULT_CAPACITY};
}

Str str_create(const char *string) {
  size_t len = strlen(string);
  char *chars = malloc(len * sizeof(char));

  strcpy(chars, string);

  Str str = {.chars = chars, .length = len, .capacity = len};
  return str;
}

void str_assign(Str *str, const char *string) {
  size_t new_len = strlen(string);
  if (new_len >= str->capacity) {
    str->chars = realloc(str->chars, new_len * sizeof(char));
    str->capacity = new_len;
  }
  memcpy(str->chars, string, new_len);
  str->length = new_len;
}

void str_append_cstr(Str *str, const char *string) {
  size_t string_len = strlen(string);
  size_t new_len = str->length + string_len;
  if (new_len >= str->capacity) {
    str->chars = realloc(str->chars, new_len * sizeof(char));
    str->capacity = new_len;
  }

  memcpy(str->chars + str->length, string, string_len);

  str->length = new_len;
}

void str_append_int(Str *str, int num) {
  int length = snprintf(NULL, 0, "%d", num);
  char *cstr = malloc((length + 1) * sizeof(char));
  snprintf(cstr, length + 1, "%d", num);
  str_append_cstr(str, cstr);
}

void str_clear(Str *str) { str->length = 0; }

void str_destroy(Str *str) {
  free(str->chars);
  str->chars = NULL;
  str->length = 0;
  str->capacity = 0;
}

bool str_to_cstr(Str *str, char *cstr, size_t n) {
  if (str->length + 1 > n) {
    return false;
  }

  memcpy(cstr, str->chars, str->length);
  cstr[str->length] = '\0';

  return true;
}

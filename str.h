#ifndef _STR_H_
#define _STR_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define STR_DEFAULT_CAPACITY 8

typedef struct Str {
  char *chars;
  size_t length;
  size_t capacity;
} Str;

Str str_create_empty();
Str str_create(const char *string);
void str_assign(Str *str, const char *string);
void str_append_cstr(Str *str, const char *string);
void str_append_int(Str *str, int32_t num);
void str_clear(Str *str);
void str_destroy(Str *str);

bool str_to_cstr(Str *str, char *cstr, size_t n);

#endif

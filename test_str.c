#include "str.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void test_assign(const char *test_str) {
  printf("Assigning const char*\n");
  Str str = str_create_empty();
  str_assign(&str, test_str);
  assert(str.length == strlen(test_str));
  assert(str.capacity >= str.length);
  assert(memcmp(str.chars, test_str, str.length) == 0);

  str_clear(&str);
  assert(str.length == 0);

  str_destroy(&str);
  assert(str.chars == NULL);
  assert(str.capacity == 0);
  assert(str.length == 0);
}

void test_append(const char *test_str) {
  printf("Appending const char*\n");
  Str str = str_create_empty();
  str_append_cstr(&str, test_str);
  assert(str.length == strlen(test_str));
  assert(str.capacity >= str.length);
  assert(memcmp(str.chars, test_str, str.length) == 0);

  char *test_appended = malloc(sizeof(char) * strlen(test_str) * 4);
  str_append_cstr(&str, test_str);
  str_append_cstr(&str, test_str);
  strcat(test_appended, test_str);
  strcat(test_appended, test_str);
  strcat(test_appended, test_str);
  assert(str.length == strlen(test_appended));
  assert(str.capacity >= str.length);
  assert(memcmp(str.chars, test_appended, str.length) == 0);

  free(test_appended);
  str_destroy(&str);
}

void test_cstr(const char *test_str) {
  printf("Converting to char*\n");
  Str str = str_create(test_str);
  str_assign(&str, test_str);
  assert(str.length == strlen(test_str));
  assert(str.capacity >= str.length);
  assert(memcmp(str.chars, test_str, str.length) == 0);

  char *cstr = malloc((str.length + 1) * sizeof(char));
  str_to_cstr(&str, cstr, str.length + 1);

  assert(strlen(cstr) == strlen(test_str));
  assert(strcmp(test_str, cstr) == 0);

  free(cstr);
  str_destroy(&str);
}

void test_num(int num) {
  printf("Appending num %d\n", num);
  Str str = str_create_empty();
  str_append_int(&str, num);

  char *cstr = malloc((str.length + 1) * sizeof(char));
  str_to_cstr(&str, cstr, str.length + 1);
  int num2 = strtol(cstr, NULL, 10);

  assert(num == num2);

  free(cstr);
  str_destroy(&str);
}

int main() {
  const char *test1 = "abc";
  const char *test2 =
      "kajshgkljashgkljasbdgklasbgklasjdbgakljsdbgklajsdbgkajlsdbg";

  printf("Test case: %s\n", test1);
  test_assign(test1);
  test_append(test1);
  test_cstr(test1);

  printf("Test case: %s\n", test2);
  test_assign(test2);
  test_append(test2);
  test_cstr(test2);

  printf("Testing numbers:\n");
  test_num(12);
  test_num(-12);
  test_num(498172);
}
